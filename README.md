# JavaAssignment

# Week1 Questions

1. Write a java program that calculates the mean, median and mode of a given array.
- double calculateMean(int[] a).
- double calculateMode(int[] a)
- double findMedian(int[] a)

 

2. Write java program that detects duplicates in an array, it should return the amount of time the item occurs in the array

# Week2 Questions
1. create an overloaded function.
    - int addNumbers(int number, int number);
    - int addNumber(List<Integer> numbers);
- write a java program that returns the sum of even and odd numbers in an array.

 

2. Create an Animal Class which is contains the following methods
    - void walkDescription(); -> describe if the animal has legs or not
    - void soundDescription(); -> describe the shows the animal makes.
    - void isDomestic(); -> shows if the animal is domestic or not
  -Create two other classes Dog class, Lion class and snake, both classes will inherit from the animal class.
  -Override those functions created in Animal class to fit the animal.
3. Read about abstract class and interfaces,  State the differences between them. 

# Week3 Questions
1. convert the animal class to an abstract class:
    - create three variables which are:
        - boolean isDomesticated.
        - string sound;
        - int legs;
        - Habitant animalHabitant;
    - make modification ton the functions:
        - String isDomestic() -> shows if the animal is domestic or not
        - String soundDescription() -> describe the shows the animal makes.
        - String walkDescription() -> describe if the animal has legs or not
        - String habitantDescription() -> describe the habitant of the animal.
        - String introduction() -> this function should return "hello, I am a "+animal Name;

 

  note: - Those functions will use the variables newly created to operate. 
           - Habitant is an enum which contains the different habitants.
           - in habitantDescription() use a switch case.

 

  - Create three other classes Dog class, Lion class and snake class, all the classes will inherit from the animal class.
  - Change the variables to fit the animal nature.

  More addition:
    - throw an exception for an invalid animalHabitant.

 

2. Create an interface Polygon which contains the following function:
    - void getArea();
    - void getSides();
- Create the following classes Triangle, Rectangle and Square. those functions should implement Polygon, modify the getArea() to calculate the area of the shape and getSides() to show the sides of each shapes.


3. Create a spring project that let you perform CRUD operation on dad's jokes. 

# Week4 Questions
1. Note: the variables in the abstract class must be private. We have to calculate the percentage of marks obtained in three subjects (each out of 100) by student A and in four subjects (each out of 100) by student B. Create an abstract class 'Marks' with an abstract method 'getPercentage'. It is inherited by two other classes 'A' and 'B' each having a method with the same name which returns the percentage of the students. The constructor of student A takes the marks in three subjects as its parameters and the marks in four subjects as its parameters for student B. Create an object for eac of the two classes and print the percentage of marks for both the students.

2. On this assignment for week 3 change all the variable access type to private. then create a setter and getter method for each variable.

- convert the animal class to an abstract class:
    - create three variables which are:
        - boolean isDomesticated.
        - string sound;
        - int legs;
        - Habitant animalHabitant;
    - make modification ton the functions:
        - String isDomestic() -> shows if the animal is domestic or not
        - String soundDescription() -> describe the shows the animal makes.
        - String walkDescription() -> describe if the animal has legs or not
        - String habitantDescription() -> describe the habitant of the animal.
        - String introduction() -> this function should return "hello, I am a "+animal Name;

 

  note: - Those functions will use the variables newly created to operate. 
           - Habitant is an enum which contains the different habitants.
           - in habitantDescription() use a switch case.

 

  Create three other classes Dog class, Lion class and snake class, all the classes will inherit from the animal class.
  Change the variables to fit the animal nature.

  More addition:
    - throw an exception for an invalid animalHabitant.

3. read on abstract class vs normal class, abstract class vs interface

# Week 5 Questions
1. Modify week 3 assignment 
-  remove the sql file
-  use lombok in the entity class

2.  create any entity mapping between author and book, and perform the following operations
- deleting a book of an author.
- updating a book of an author.
- add a book to an author.
- view a book

# Week 6 Questions
1. This code challenge is designed to assess your general understanding of RESTful API principles and software development in Java. You will be accessed based on your ability to:
   • Develop clean, reusable and maintainable features;
   • Follow best practices for coding styles and conventions; and
   • Efficiently retrieve and manipulate data.
   Feature Requirements
   The list of required features are as follows:
- Add Category
- Edit Category
- List Categories
- Add Book
- Edit Book
- List Books
- Add Books to category
2. Additional good-to-have:
- Create list of favourite books
- Delete Category/Book
- API documentation