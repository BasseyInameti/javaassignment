package javaAssignment.dadJokes.services;

import javaAssignment.dadJokes.domains.Jokes;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("JokeServiceImp")
public class JokeServiceImp implements JokeService{
    ArrayList <Jokes> jokesArrayList = new ArrayList<>();//storing the data

    @Override
    public void save(Jokes jokes) { // saving the data to the array
        jokes.setId((long) jokesArrayList.size());//set id to
        jokesArrayList.add(jokes);
        System.out.println(jokes);
    }
    @Override
    public void update(int id, Jokes jokes){
        jokes.setId((long) id);//set the id
        jokesArrayList.set(id, jokes);

    }

    @Override
    public List<Jokes> findAll() { //return the list

        return jokesArrayList;
    }

    @Override
    public Jokes findById(int id) throws Exception {
        if ((id )>jokesArrayList.size()){
            throw new Exception("the id does not exist");
        }
       return jokesArrayList.get(id);
    }

    @Override
    public void deleteById(int id) {
        jokesArrayList.remove(id);
    }
}
