package javaAssignment;

public interface Polygon {
    void getArea();
    void getSides();
}

class Rectangle implements Polygon{
    private int length, width;
    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public void getArea() {
        int area= length*width;
        System.out.println("the area of this square is "+ area);
        
    }

    @Override
    public void getSides() {
        
        System.out.println("a rectangle has 4 sides");
    }
    
}

class Square implements Polygon{
    private int sideLength;

    public Square(int sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public void getArea() {
        int area= sideLength*sideLength;
        System.out.println("the area of this square is "+ area);
    }

    @Override
    public void getSides() {
        
        System.out.println("a square has 4 sides");
    }
    
}

class Triangle implements Polygon{
    private int base,height;

    public Triangle(int base, int height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public void getArea() {
        double area =0;
        area=(0.5*base*height);
        System.out.println("the area of the triangle is "+ area);
        
    }

    @Override
    public void getSides() {
        
        System.out.println("a triangle has 3 sides");
        
    }
    
}