package javaAssignment.dadJokes.services;

import javaAssignment.dadJokes.domains.Jokes;

import java.util.List;

public interface JokeService {
    void save(Jokes jokes);
    List<Jokes> findAll();
    void update(int id, Jokes jokes);
    Jokes findById(int id) throws Exception;
    void deleteById(int id);


}
