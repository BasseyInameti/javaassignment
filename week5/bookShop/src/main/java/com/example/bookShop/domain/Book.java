package com.example.bookShop.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter @Getter
    @EqualsAndHashCode.Include
    private Long id;

    @Getter @Setter
    private String title;
    @Setter @Getter
    private String isbn;
    @Setter @Getter
    @ManyToMany
    @JoinTable(name = "author_book", joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private Set<Author> authors = new HashSet<>();


    public Book(String title, String isbn) {
        this.title = title;
        this.isbn = isbn;
    }




}
