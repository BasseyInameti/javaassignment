package com.example.mangareader.entity;
// Todo 1: create a category entity


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

// TODO 3: add all the necessary annotations



@Data
@NoArgsConstructor
@Entity
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Category {
    // Todo 2: add the required variables


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;
    private String name;

    @ManyToMany
    @JoinTable(name ="categories_books", joinColumns =@JoinColumn(name = "category_id"),
                inverseJoinColumns= @JoinColumn(name ="book_id" ))
    private Set<Book> book = new HashSet<>();


}
