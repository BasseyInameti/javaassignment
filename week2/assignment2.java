package javaAssignment;

import java.util.ArrayList;

public class assignment2 {
    public int addNumber(int number ,int number1){
        int total=0;
        total= number + number1;
        return total;
    }
    
    public int addNumber(ArrayList< Integer > numbers){
        int total=0;
        for (int num : numbers){
         
          total += num;
        }
        return total;
    }

    public int oddNumber(int[] num){
        int total =0;
        for (int number : num)
        {
            if (number%2 != 0)
            {
                total += number;
            }
    }
        return total;
    }

    public int evenNumber(int[] num){
        int total =0;
        for (int number : num){
        if (number%2 == 0)
        {
            total += number;
        }
    }
        return total;
    }
}

class Animal{

    void walkDescription(){
        System.out.println("this animal has legs ");
    }

    void soundDescription(){
        System.out.println("all animals make sound");
    }

    void isDomestic(){
        System.out.println("most animals are not domesticated");
    }

}

class Dog extends Animal{

    
    void isDomestic() {
        System.out.println("this is domesticated");
    }

    
    void soundDescription() {
        System.out.println("this animal barks");
    }

   
    void walkDescription() {
        System.out.println("it has 4 legs");
    }
 
}
class Lion extends Animal{

    
    void isDomestic() {
        System.out.println("it's not domesticated");
    }

   
    void soundDescription() {
        System.out.println("it roars");
    }

    
    void walkDescription() {
        System.out.println("it has four legs");
    }



}
class Snake extends Animal{
    void isDomestic() {
        System.out.println("it's not domesticated");
    }

   
    void soundDescription() {
        System.out.println("it hisses");
    }

    
    void walkDescription() {
        System.out.println("it has no legs");
    }

}