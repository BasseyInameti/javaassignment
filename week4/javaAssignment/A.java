package javaAssignment;

public class A extends Marks{
    private int subject1;
    private int subject2;
    private int subject3;

    public A(int subject1, int subject2, int subject3) {
        this.subject1 = subject1;
        this.subject2 = subject2;
        this.subject3 = subject3;
    }
    @Override
    float getPercentage() {
        float percentage= (subject1+subject2+subject3)/300;
        return percentage;
    }
    
}
class B extends Marks{
    private int subject1;
    private int subject2;
    private int subject3;
    private int subject4;

    public B(int subject1, int subject2, int subject3, int subject4) {
        this.subject1 = subject1;
        this.subject2 = subject2;
        this.subject3 = subject3;
        this.subject4 = subject4;
    }


    @Override
    float getPercentage() {
        float percentage= (subject1+subject2+subject3+subject4)/400;
        return percentage;
    }
    
}