package com.example.mangareader.controller;

import com.example.mangareader.dto.BookDTO;
import com.example.mangareader.dto.CategoryDTO;
import com.example.mangareader.dto.UserDTO;
import com.example.mangareader.entity.Book;
import com.example.mangareader.entity.Category;
import com.example.mangareader.entity.Users;
import com.example.mangareader.repository.BookRepository;
import com.example.mangareader.repository.CategoryRepository;
import com.example.mangareader.repository.UsersRepo;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@AllArgsConstructor
public class Controller {
    private final BookRepository bookRepository;
    private final CategoryRepository categoryRepository;
    private final UsersRepo usersRepo;
    private ModelMapper modelMapper;

    @GetMapping("/category")//list all categories
    public ArrayList<CategoryDTO> getAllCategories(){
        List<Category> categories = categoryRepository.findAll();
        ArrayList<CategoryDTO> categoryDto = new ArrayList<CategoryDTO>();
        for (Category category: categories
             ) {
            categoryDto.add(modelMapper.map(category,CategoryDTO.class));
        }
        return categoryDto;
    }

    @GetMapping("/book")
    public List<BookDTO> getAllBooks(){
        //return all the book objects in a list
        List<Book> books = bookRepository.findAll();
        ArrayList<BookDTO> bookDto = new ArrayList<BookDTO>();
        for (int i=0; i<books.size(); i++){
            bookDto.add(modelMapper.map(books.get(i),BookDTO.class));
        }
        return bookDto;
    }

    @GetMapping("/book/")
    public BookDTO getBook(@RequestParam("book_id")int bookId) throws Exception {
        Optional<Book> book = bookRepository.findById(bookId);
        if (!book.isPresent()){
            throw new Exception("book does not exist");
        }
        return modelMapper.map(book,BookDTO.class);
    }

    @GetMapping("/category/")
    public CategoryDTO getCategoryByName(@RequestParam("category_name") String category_name){
        //returns category with the name searched for

        Optional<Category> category= categoryRepository.findCategoriesByNameContainingIgnoreCase(category_name);
        if (category != null){
            return modelMapper.map(category,CategoryDTO.class);
        }else {
            throw new RuntimeException("this category does not exist");
        }

    }

    @GetMapping("/user")
    public ArrayList<UserDTO> getAllUser(){
        
        List<Users> users= usersRepo.findAll();
        ArrayList<UserDTO> userDTOS= new ArrayList<UserDTO>();
        for (Users user: users
             ) {
            userDTOS.add(modelMapper.map(user, UserDTO.class));

        }
        return userDTOS;
    }

    @PostMapping("/user")
    public String addUser(@RequestBody Users users){
        usersRepo.save(users);
        return "added Successfully";
    }

    @PostMapping("/category")//add a category
    public String addCategory(@RequestBody Category category){
        if (categoryRepository.findCategoriesByNameContainingIgnoreCase(category.getName()).isPresent()){
            throw new  RuntimeException("sorry this category already exists");
        }
        //add a category object to the database
        categoryRepository.save(category);
        return "successfully added a category";
    }
    @PostMapping("/book/")
    public String addBook(@RequestBody Book book){
        //add a book object to the database
        bookRepository.save(book);
        return "successfully added a book";
    }

    @PutMapping("/updateCategory/")//updating a category name by using it id
    public String updateCategory(@RequestParam("id") int id, @RequestBody Category category){
        //updates the name of the category
        Category oldCategory= categoryRepository.findById(id).get();
        oldCategory.setName(category.getName());
        categoryRepository.save(oldCategory);
        return "update successful";
    }

    @PutMapping("/updateBook/")
    public String updateBook(@RequestParam("id") int id, @RequestBody Book book){
        //update the name of a book
        Book oldBook = bookRepository.getReferenceById(id);
        oldBook.setTitle(book.getTitle());
        bookRepository.save(oldBook);
        return "update successful";
    }

    @PutMapping("/updateCategoryBook/")
    public String addBooksToCategory(@RequestParam("book_id") int book_id,@RequestParam("category_id") int category_id){
        //add a book to category
        Book book=bookRepository.getReferenceById(book_id);
        Category category= categoryRepository.getReferenceById(category_id);

        category.getBook().add(book);

        categoryRepository.save(category);
        return "Successfully added";
    }

    @SneakyThrows
    @PutMapping("/addFavorite/")
    public String addBookToFavorite(@RequestParam("user_id") int user_id, @RequestParam("book_id") int book_id){
        Book book = bookRepository.findById(book_id).get();
        Users user = usersRepo.findById(user_id).get();
        user.getFavBook().add(book);
        usersRepo.save(user);
        return "Successfully added a book to the user";
    }
    @DeleteMapping("/deleteCategory/")//delete a category by using its id
    public String deleteCategory(@RequestParam("id") int id){
        categoryRepository.deleteById(id);
        return "successfully deleted";
    }

    @DeleteMapping("/deleteBook")
    public String deleteBook(@RequestParam("id") int id){
        bookRepository.deleteById(id);
        return "successfully deleted";
    }

}
