package com.example.bookShop.service;

import com.example.bookShop.domain.Author;

import java.util.List;

public interface AuthorServices {
    List findAll();

    Author findById(int id);
    void save(Author author);
    void deleteById(int id);

    void update(Author author, int id);
}
