package javaAssignment;

/**
 * Dog
extends */


public class assignment3Runner {
    public static void main(String[] args) {
        Dog dog = new Dog(true,"barks",4);

        dog.introduceAnimal();
        System.out.println(dog.habitantDescription("terrestrial"));
        
        Snake snake = new Snake(false,"hisses",0);
        snake.introduceAnimal();
        System.out.println(snake.habitantDescription("aquatic"));

        Lion lion = new Lion(false,"roars",4);
        System.out.println(lion.introduction());
        // testing out the try_catch
        System.out.println(lion.habitantDescription("savannah"));


                
    }
    
}
