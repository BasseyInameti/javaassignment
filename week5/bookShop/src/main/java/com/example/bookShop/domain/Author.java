package com.example.bookShop.domain;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@ToString
@NoArgsConstructor
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "id")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter @Getter
    @EqualsAndHashCode.Include
    private long id;
    @Setter @Getter
    private String firstName;
    @Setter @Getter
    private String lastName;
    @ManyToMany(mappedBy = "authors")
    @Setter @Getter
    private Set<Book> books = new HashSet<>();

    public Author(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

    }




}
