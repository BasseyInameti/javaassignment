package javaAssignment;

abstract class Marks {

    private int subject1;
    private int subject2;
    private int subject3;

    abstract float getPercentage();
}