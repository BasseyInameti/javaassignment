package com.example.bookShop.service;



import com.example.bookShop.domain.Book;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookServiceImp implements BookService {
    ArrayList<Book> bookList = new ArrayList<>();
    @Override
    public List findAll() {
        return bookList;
    }

    @Override
    public Book findById(int id) {
      try {
          return bookList.get(id);
      } catch (Exception e){
          throw new IndexOutOfBoundsException("This id has nothing there");
      }

    }

    @Override
    public void save(Book book) {
        book.setId((long) bookList.size());
        bookList.add(book);
    }

    @Override
    public void deleteById(int id) {
        bookList.remove(id);
    }

    @Override
    public void update(Book book, int id) {
        bookList.set(id,book);

    }
}
