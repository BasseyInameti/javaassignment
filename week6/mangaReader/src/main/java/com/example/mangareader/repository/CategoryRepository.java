package com.example.mangareader.repository;

import com.example.mangareader.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface CategoryRepository extends JpaRepository<Category,Integer> {
    Optional<Category> findCategoriesByNameContainingIgnoreCase(String name);

}
