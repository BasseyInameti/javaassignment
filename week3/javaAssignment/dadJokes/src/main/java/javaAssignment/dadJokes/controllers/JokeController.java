package javaAssignment.dadJokes.controllers;

import javaAssignment.dadJokes.domains.Jokes;
import javaAssignment.dadJokes.services.JokeService;
import javaAssignment.dadJokes.services.JokeServiceImp;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@AllArgsConstructor
@RestController
public class JokeController {

    private final JokeService jokeService;


    @GetMapping(value = "/jokes")
    public List<Jokes> getAllJokes(){
        return  jokeService.findAll();
    }

    @GetMapping("/specificJokes")
    @ResponseBody
    public Jokes getSpecificJoke(@RequestParam("id") int id) throws Exception {
        return jokeService.findById(id);
    }
    @PostMapping(value = "/newJoke")
    public String addJoke(@RequestBody Jokes jokes){
        jokeService.save(jokes);
        return "Successfully added the joke" ;
    }

    @SneakyThrows
    @ResponseBody
    @PutMapping("/updateJoke")
    public String updateJokes(@RequestParam("id") int id,@RequestBody Jokes jokes)  {
        Jokes oldJoke = jokeService.findById(id);
        if (jokes.getJokes() != null){
            oldJoke.setJokes(jokes.getJokes());
        }
        if (jokes.getDad() != null){
            oldJoke.setDad(jokes.getDad());
        }
        jokeService.update(id,oldJoke);
        return "Successfully added the joke" ;
    }

    @DeleteMapping("/deleteJokes")
    @ResponseBody
    public String deleteJoke(@RequestParam("id") int id){
        jokeService.deleteById(id);
        return "Successfully deleted the joke";
    }
}
