package com.example.bookShop.service;

import com.example.bookShop.domain.Author;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class AuthorServiceImp implements AuthorServices {
    ArrayList<Author> authorList =new ArrayList<>();

    @Override
    public List<Author> findAll() {
        return authorList;
    }

    @Override
    public Author findById(int id) {
        try{
        return authorList.get(id);
        } catch (Exception e){
            throw new IndexOutOfBoundsException("this author does not exist");
        }

    }

    @Override
    public void save(Author author) {
        author.setId((long) authorList.size());
        authorList.add(author);
    }

    @Override
    public void deleteById(int id) {
        authorList.remove(id);
    }

    @Override
    public void update(Author author,int id) {
        authorList.set(id,author);
    }
}
