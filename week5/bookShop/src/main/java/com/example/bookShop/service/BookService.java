package com.example.bookShop.service;

import com.example.bookShop.domain.Book;

import java.util.List;


public interface BookService {
    List findAll();
    Book findById(int id);
    void save(Book Book);
    void deleteById(int id);

    void update(Book book, int id);
}
