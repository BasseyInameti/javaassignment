package javaAssignment;

public abstract class Animal {
    private boolean isDomesticated;
    private String sound;
    private int legs;
    private Habitant animalHabitant;

    public Animal(boolean isDomesticated, String sound, int legs) {
        this.isDomesticated = isDomesticated;
        this.sound = sound;
        this.legs = legs;
    }
        
    String walkDescription(){
        return("this animal has "+this.legs+ " legs ");
    }

    String soundDescription(){
        return("this animal "+this.sound );
    }

    String isDomestic(){
        if (this.isDomesticated == true){
            return("this animal is domesticated");
        }
        else{
            return("this animal is not domesticated");
        }
    }

    String habitantDescription(String habitat){
        String message="";
        try {
            switch (Habitant.valueOf(habitat) ) {
            case aquatic:

                message=("it's an aquatic organism");                
                break;

            case terrestrial:
                message=("it's a land animal");
           
        } 
        } catch (Exception e) {
            // TODO: handle exception
            message=("the habitat you have chosen is not accepted pick either terrestrial or aquatic");
        }
        
        return message;


    }

    String introduction() {
        return("hello i'm animal");
    }

    void introduceAnimal(){
        System.out.println(introduction());
        System.out.println(walkDescription());
        System.out.println(isDomestic());
    }
}

class Dog extends Animal{

 

    public Dog(boolean isDomesticated, String sound, int legs) {
        super(isDomesticated, sound, legs);
        
    }

    @Override
    String introduction() {
        
        return  ("hello i'm a dog" );
    }
   

    
}
/**
 * Snake
 */
class Snake extends Animal {

    public Snake(boolean isDomesticated, String sound, int legs) {
        super(isDomesticated, sound, legs);
        //TODO Auto-generated constructor stub
    }

    @Override
    String introduction() {
        // TODO Auto-generated method stub
        return ("hello i'm a snake" );
    }

    
}

class Lion extends Animal{

 

    public Lion(boolean isDomesticated, String sound, int legs) {
        super(isDomesticated, sound, legs);
        
    }

    @Override
    String introduction() {
        
        return  ("hello i'm a lion" );
    }
   

    
}