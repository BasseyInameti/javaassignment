package javaAssignment;
import java.util.Arrays;

public class assignment1 {
    public double calculateMean(int[] a){
       
        double mean=  IntStream.of(a).average().getAsDouble();/*  using functional programming to solve it
        the intStream has the method to calculate average, it returns an optional double ,
        the getAsDouble solve that return problem*/
        return  Math.round(mean * 100.0) / 100.0;
    }
    public  double findMedian(int[] a){
        double median=0;
        Arrays.sort(a);

        double middleNumber = ((double) a.length) /2;
        if ((a.length % 2) != 0 ){
            median=a[(int) middleNumber];
            
        }else{
            median= ((a[ ((int)middleNumber)-1] + a[ (int)middleNumber])/2);
            
        }

        return median;
    }

    public Double calculateMode(int a[]) {
        double maxValue = 0, maxCount = 0, n = a.length;
        for (int i = 0; i < (n); ++i) 

        {
            int count = 0;
            for (int j = 0; j < n; ++j){

                if (a[j] == a[i]){
                    ++count;   
                }
            }

            if (count > maxCount){
                // System.out.println("this is"+maxValue);
                maxCount = count;
                maxValue = a[i];
            }
        }
        return maxValue;
    }

    public void findRepetition(int[] a) {
        Arrays.sort(a);
        int arrayLength = a.length;
        int freq[] = new int[arrayLength];//length of new array

        for (int i=0; i<arrayLength; i++){
            int count =0;
            for (int j =0; j< arrayLength; ++j){
                if (a[j]==a[i]){
                    ++count;
                    ++freq[i];
                }
                
            }
        }
        for (int j=0; j<freq.length - 1; j++){
            if (a[j] != a[j+1]){  
                System.out.println(a[j]+"\t"+freq[j]);
            }        
        }
    }

}
