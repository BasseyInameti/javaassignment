package javaAssignment;

public class interfaceRunner {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(5,9);
        Square square = new Square(6);
        Rectangle rectangle=new Rectangle(4,6);
        
        square.getArea();
        square.getSides();

        triangle.getArea();
        triangle.getSides();

        rectangle.getArea();
        rectangle.getSides();
    }
}
