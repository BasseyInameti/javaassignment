package com.example.mangareader.entity;

import lombok.*;

import javax.persistence.*;
import javax.persistence.Id;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter @Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Include
    private int id;

    @Column(unique = true)
    private String title;

    @ManyToMany(mappedBy = "book")
    private Set<Category> categorySet = new HashSet<>();

    public Book(int id, String title) {
        this.id = id;
        this.title = title;
    }

}
