package com.example.bookShop.controller;

import com.example.bookShop.domain.Author;
import com.example.bookShop.domain.Book;
import com.example.bookShop.service.AuthorServices;
import com.example.bookShop.service.BookService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ServiceController {

    private final AuthorServices authorService;

    private final BookService bookService;

    @GetMapping("/authors")
    public List getAllAuthor() {
        return authorService.findAll();
    }

    @GetMapping("/books")
    public List getAllBooks(){
        return bookService.findAll();
    }

    @PostMapping("/addAuthor")
    public String addAuthor(@RequestBody Author author){
        authorService.save(author);
        return "Successfully added an author";
    }

    @PostMapping("/addBook")
    public String addBook(@RequestBody Book book){
        bookService.save(book);
        return "Successfully added a book";
    }

    @PutMapping("/addBookToAuthor")
    @ResponseBody
    public String addBookToAuthor(@RequestParam("author_id") int author_id,@RequestParam("book_id") int book_id){
        Author author = authorService.findById(author_id);
        Book book=bookService.findById(book_id);
        if (author != null || book != null) {

            author.getBooks().add(book);
            book.getAuthors().add(author);

            authorService.update(author, author_id);
            bookService.update(book,book_id);

        }else {
            throw new ArrayIndexOutOfBoundsException("This author id or book id does not exist");
        }


       return  "Successfully added a book to the author";
    }

     @PutMapping("/updateABook")
     @ResponseBody
     public String updateBook(@RequestBody Book book, @RequestParam("book_id") int book_id){
        try {
            Book oldBook = bookService.findById(book_id);
            if (book.getTitle()!=null){
                oldBook.setTitle(book.getTitle());
            }
            if (book.getIsbn() != null){
                oldBook.setIsbn(book.getIsbn());
            }

            bookService.update(oldBook,book_id);

        } catch (IndexOutOfBoundsException e){
            bookService.save(book);
            return "The book you tried to update did not exist so i created an new book for you";
        }
        return "Successful updated the book";
     }

     @DeleteMapping("/deleteABook")
     @ResponseBody
     public String deleteBookFromAuthor(@RequestParam("book_id") int book_id,@RequestParam("author_id") int author_id){
        Book book = bookService.findById(book_id);
        Author author = authorService.findById(author_id);

        author.getBooks().remove(book);
        bookService.deleteById(book_id);
        authorService.update(author,author_id);//


        return "Successfully deleted the book";
     }
}
