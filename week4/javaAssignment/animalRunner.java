package javaAssignment;

/**
 * Dog
extends */


public class animalRunner {
    public static void main(String[] args) throws Exception {
        Dog dog = new Dog(true,"barks",4);

        dog.introduceAnimal();
        dog.setAnimalHabitant(Habitant.terrestrial);
        System.out.println(dog.habitantDescription());
        
        Snake snake = new Snake(false,"hisses",0);
        snake.introduceAnimal();
        snake.setAnimalHabitant(Habitant.aquatic);
        System.out.println(snake.habitantDescription());

        Lion lion = new Lion(false,"roars",4);
        System.out.println(lion.introduction());
        // testing out the try_catch
        
        System.out.println(lion.habitantDescription());


                
    }
    
}
