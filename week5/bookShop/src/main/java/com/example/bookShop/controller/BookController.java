//package com.example.bookShop.controller;
//
//import com.example.bookShop.domain.Author;
//import com.example.bookShop.domain.Book;
//import com.example.bookShop.repository.AuthorRepository;
//import com.example.bookShop.repository.BookRepository;
//import lombok.AllArgsConstructor;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@AllArgsConstructor
//public class BookController  {
//
//    private final BookRepository bookRepository;
//    private final AuthorRepository authorRepository;
//
//    @GetMapping("/books")
//    public List<Book> getAllBooks(){
//        return bookRepository.findAll();
//    }
//    @GetMapping("/authors")
//    public List<Author> getAllAuthors(){
//        return authorRepository.findAll();
//    }
//
//    @GetMapping("/author/{id}")
//    public Author getSpecificAuthor(@PathVariable long id){
//        return authorRepository.findById(id).get();
//
//    }
//
//    @PostMapping("/books/{authorId}")
//    public String addBooksToAuthor(@RequestBody Book book,@PathVariable Long authorId){
//
//        if (!authorRepository.findById(authorId).isPresent()){
//            throw new RuntimeException("Sorry we don't have an author with that Id "+ authorId);
//        }
//           Author author = authorRepository.findById(authorId).get();
//
//        book.getAuthors().add(author);
//        author.getBooks().add(book);
//        bookRepository.save(book);
//        authorRepository.save(author);
//
//
//        return ("Successfully added a book");
//    }
//
//    @PostMapping("/author")
//    public String addAuthor(@RequestBody Author author){
//        authorRepository.save(author);
//        return "Successfully added an author";
//    }
//
//    @PutMapping("book/{bookId}")
//    public String updateABook(@PathVariable Long bookId, @RequestBody Book book ){
//        Book oldBook = bookRepository.findById(bookId).get();
//        if (!bookRepository.findById(bookId).isPresent()){
//            throw new RuntimeException("Sorry we don't have an author with that Id "+ bookId);
//        }
//        if (book.getTitle() != null){
//           oldBook.setTitle(book.getTitle());
//        }        if (book.getIsbn() != null) {
//            oldBook.setTitle(book.getIsbn());
//        }
//
//
//
//        bookRepository.save(oldBook);
//        return "Successfully Updated The Book";
//    }
//
//    @DeleteMapping("/deleteBook/{bookId}")
//    public String deleteABook(@PathVariable Long bookId){
//        bookRepository.deleteById(bookId);
//
//        return "Successfully Deleted";
//    }
////    public String getBooks(/*Model model*/ ){
//////        model.addAttribute("books",bookRepository.findAll());
//////        return "books";
////    }
//}
