package javaAssignment.dadJokes.repository;

import javaAssignment.dadJokes.domains.Jokes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JokeRepository extends JpaRepository<Jokes,Long> {
}
